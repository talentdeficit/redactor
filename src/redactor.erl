%% The MIT License
%%
%% Copyright (c) 2013 alisdair sullivan <alisdairsullivan@yahoo.ca>
%%
%% Permission is hereby granted, free of charge, to any person obtaining a copy
%% of this software and associated documentation files (the "Software"), to deal
%% in the Software without restriction, including without limitation the rights
%% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%% copies of the Software, and to permit persons to whom the Software is
%% furnished to do so, subject to the following conditions:
%%
%% The above copyright notice and this permission notice shall be included in
%% all copies or substantial portions of the Software.
%%
%% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%% THE SOFTWARE.


-module(redactor).


%% bash script entry point
-export([red/0]).
%% profile api
-export([get_value/1, get_value/2, new_profile/2]).
%% otp callbacks
-export([start/2, start/0, stop/1, init/1, terminate/2]).
-export([handle_call/3]).
-export([redactor_profiles/0]).
%% logging
-export([error/1, warn/1, info/1, debug/1]).




%% let's not get crazy! for now just print out the args to `red' and halt
red() -> io:format("~p~n", [init:get_plain_arguments()]), halt(0).


%% profile api

-spec get_value(atom()) -> {ok, term()} | undefined.

get_value(Value) when is_atom(Value) -> get_value(Value, [defaults]).

-spec get_value(atom(), [atom()]) -> {ok, term()} | undefined.

get_value(Value, Ps) when is_atom(Value), is_list(Ps) ->
    gen_server:call(redactor_profiles, {get_value, Value, Ps}).

-spec new_profile(atom(), term()) -> ok | {error, term()}.

new_profile(Name, Profile) when is_atom(Name), is_list(Profile) ->
    gen_server:call(redactor_profiles, {add_profile, Name, Profile}).
    


lookup(_, []) -> undefined;
lookup(Value, [Profile|Rest]) ->
    case proplists:get_value(Value, Profile) of
        undefined -> lookup(Value, Rest);
        List when is_list(List) ->
            case io_lib:char_list(List) of
                true -> {ok, List};
                false -> lookup(Value, Rest, List)
            end;
        NonList -> {ok, NonList}
    end.

lookup(_, [], Result) -> {ok, Result};
lookup(Value, [Profile|Rest], Result) ->
    case proplists:get_value(Value, Profile) of
        undefined -> lookup(Value, Rest, Result);
        List when is_list(List) ->
            false = io_lib:char_list(List),
            lookup(Value, Rest, Result ++ List)
    end.


insert(File, Ps) ->
    {ok, [{profile, Name, Profile}]} = file:consult(File),
    insert(Name, Profile, Ps).

insert(Name, Profile, Ps) -> gb_trees:insert(Name, Profile, Ps).


extract(Profiles, Ps) -> [ gb_trees:get(P, Ps) || P <- Profiles ].


%% application machinery
handle_call({get_value, Value, Profiles}, _, Ps) -> {reply, lookup(Value, extract(Profiles, Ps)), Ps};
handle_call({add_profile, Name, Profile}, _, Ps) -> {reply, ok, insert(Name, Profile, Ps)}.

start() -> application:start(redactor).

start(_Type, _Args) -> redactor_supervisor().

stop(_State) -> ok.

%% application internal callbacks
redactor_supervisor() -> supervisor:start_link(redactor, {supervisor, []}).

redactor_profiles() ->
    gen_server:start_link({local, redactor_profiles}, redactor, {redactor_profiles, []}, []).

%% otp boilerplate sucks
init({supervisor, _Args}) ->
    {ok, {
        {one_for_one, 1, 5},
        [{profiles, {?MODULE, redactor_profiles, []}, permanent, brutal_kill, worker, [?MODULE]}]
    }};
init({redactor_profiles, _Args}) ->
    {ok, insert(code:priv_dir(redactor) ++ "/defaults.config", gb_trees:empty())}.

terminate(_, _) -> ok.


%% logging functions
warn(Msg) -> log(warn, Msg).
error(Msg) -> log(error, Msg).
info(Msg) -> log(info, Msg).
debug(Msg) -> log(debug, Msg).

log(_Level, _Msg) -> noop.



-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

tty(OnOff) -> error_logger:tty(OnOff).


application_start_test_() ->
    {setup, fun() -> tty(false) end, fun(_) -> ok = application:stop(redactor), tty(true) end, [
        {"application starts cleanly", ?_assertEqual(ok, application:start(redactor))}
    ]}.

application_stop_test_() ->
    {setup, fun() -> tty(false), ok = application:start(redactor) end, fun(_) -> tty(true) end, [
        {"application stops cleanly", ?_assertEqual(ok, application:stop(redactor))}
    ]}.

default_profile_test_() ->
    {setup,
        fun() -> tty(false), ok = application:start(redactor) end,
        fun(_) -> application:stop(redactor), tty(true) end,
        [
            {"undefined key", ?_assertEqual(undefined, get_value(foo))},
            {"all default dependencies", ?_assertEqual({ok, [stdlib, kernel]}, get_value(dependencies))},
            {"default build dir", ?_assertEqual({ok, "build"}, get_value(build_dir))}
        ]
    }.

multiple_profiles_test_() ->
    {setup,
        fun() -> tty(false), ok = application:start(redactor) end,
        fun(_) -> application:stop(redactor), tty(false) end,
        [
            {"load profile", ?_assertEqual(
                ok,
                new_profile(test, [{dependencies, [redactor]}, {build_dir, "_build"}])
            )},
            {"composite value", ?_assertEqual(
                {ok, [redactor, stdlib, kernel]},
                get_value(dependencies, [test, defaults])
            )},
            {"replaced value", ?_assertEqual(
                {ok, "_build"},
                get_value(build_dir, [test, defaults])
            )}
        ]
    }.


-endif.